/*
 * This class implements a Tomcat Realm based on MongoDB
 */
package dk.alexandra.tomcatrealm;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import java.net.UnknownHostException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.catalina.realm.GenericPrincipal;

/**
 *
 * @author fah
 */
public class TomcatMongoRealm extends org.apache.catalina.realm.RealmBase {
    private String mongo_server = null;
    private int mongo_port = 0;
    private String mongo_database = null;
    private String mongo_collection = null;
    private String mongo_user = null;
    private String mongo_password = null;
    private String mongo_roles = null;
    
    private DB _db;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Test data
        TomcatMongoRealm realm = new TomcatMongoRealm();
        realm.setMongoServer("127.0.0.1");
        realm.setMongoPort(27017);
        realm.setMongoDatabase("STORE_EVAL_DB");
        realm.setMongoCollection("users");
        realm.setMongoUser("username");
        realm.setMongoPassword("password");
        realm.setMongoRoles("roles");
    
        System.out.println("pwd: " + realm.getPassword("mongo"));
        System.out.println("roles: " + realm.getRoles("mongo"));
        
        System.out.println("pwd: " + realm.getPassword("admin"));
        System.out.println("roles: " + realm.getRoles("admin"));
    }

    /**
     * Return a short name for this Realm implementation.
     */
    @Override
    protected String getName() {
        return "Alexandra-TomcatMongoRealm";
    }

    /**
     * Return the password associated with the given principal's user name.
     */
    @Override
    protected String getPassword(String username) {
        DBObject user = getMongoUserObject(username);
        
        if (user!=null) {
            return (String)user.get(getMongoPassword());
        }
        
        return null;
    }

    /**
     * Return the Principal associated with the given user name.
     */
    @Override
    protected Principal getPrincipal(String username) {
        return (new GenericPrincipal(username, getPassword(username), getRoles(username)));
    }
    
    /**
     * Return the roles associated with the gven user name.
     */
    protected List<String> getRoles(String username) {
        List<String> roles = new ArrayList<String>();
        
        DBObject user = getMongoUserObject(username);
        if (user!=null) {
            Object get = user.get(getMongoRoles());
        }
        
        if (user!=null && user.get(getMongoRoles())!=null){
            BasicDBList roleList =  (BasicDBList) user.get(getMongoRoles());
            
            for (Object role : roleList) { 
                roles.add((String)role);
            }
        }
        
        return roles;
    }
    
    private DB getMongoDB() {
        if (_db!=null) {
            return _db;
        }
        
        MongoClient mongoClient;
        try {
            mongoClient = new MongoClient(getMongoServer(), getMongoPort());
            // or, to connect to a replica set, with auto-discovery of the primary, supply a seed list of members
            //new ServerAddress("localhost", 27019)));
            //new ServerAddress("localhost", 27018),
            //new ServerAddress("localhost", 27019)));
            
            _db = mongoClient.getDB(getMongoDatabase());
        } catch (UnknownHostException ex) {
            Logger.getLogger(TomcatMongoRealm.class.getName()).log(Level.SEVERE, null, ex);
        }

        return _db;
    }
    
     private DBObject getMongoUserObject(String username) {
        DB db = getMongoDB();
        DBCollection collection = db.getCollection(getMongoCollection());
        BasicDBObject query = new BasicDBObject("username", username);
        DBObject doc = collection.findOne(query);
        return doc;
    }
     
    public void setMongoServer(String server) {
        mongo_server = server;
    }
    
    public String getMongoServer() {
        return mongo_server;
    }
    
    public void setMongoPort(int port) {
        mongo_port = port;
    }
    
    public int getMongoPort() {
        return mongo_port;
    }
    
    public void setMongoUser(String name) {
        mongo_user = name;
    }
    
    public String getMongoUser() {
        return mongo_user;
    }
    
    public void setMongoPassword(String pwd) {
        mongo_password = pwd;
    }
    
    public String getMongoPassword() {
        return mongo_password;
    }
    
    public void setMongoRoles(String roles) {
        mongo_roles = roles;
    }
    
    public String getMongoRoles() {
        return mongo_roles;
    }
    
    public void setMongoCollection(String collection) {
        mongo_collection = collection;
    }
    
    public String getMongoCollection() {
        return mongo_collection;
    }
    
    public void setMongoDatabase(String db) {
        mongo_database = db;
    }
    
    public String getMongoDatabase() {
        return mongo_database;
    }
}
